-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Квт 07 2017 р., 10:49
-- Версія сервера: 10.1.21-MariaDB
-- Версія PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `test_work`
--

-- --------------------------------------------------------

--
-- Структура таблиці `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` text,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `partners`
--

INSERT INTO `partners` (`id`, `name`, `deleted`) VALUES
(1, 'Main', 0),
(2, 'PART2', 0),
(3, 'PART3', 0),
(4, 'PART4', 0),
(5, 'PART1', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `statistics`
--

CREATE TABLE `statistics` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `tariff_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `nas_id` int(11) DEFAULT NULL,
  `login` text,
  `in_bytes` int(20) DEFAULT NULL,
  `out_bytes` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `start_time` text,
  `end_date` datetime DEFAULT NULL,
  `end_time` text,
  `ipv4` text,
  `ipv6` text,
  `mac` text,
  `call_to` text,
  `port` text,
  `error` int(11) DEFAULT NULL,
  `error_repeat` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `session_id` text,
  `terminate_cause` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `statistics`
--

INSERT INTO `statistics` (`id`, `customer_id`, `service_id`, `tariff_id`, `partner_id`, `nas_id`, `login`, `in_bytes`, `out_bytes`, `start_date`, `start_time`, `end_date`, `end_time`, `ipv4`, `ipv6`, `mac`, `call_to`, `port`, `error`, `error_repeat`, `price`, `session_id`, `terminate_cause`) VALUES
(12, 1, 56, 1, 1, 0, 'pavloPenrenko', 11473647, 896576576, '2017-03-07 00:00:00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', '', '', '', '', '', 0, 0, 0, '', 0),
(13, 1, 67, 2, 1, 0, 'pavloPenrenko1', 21474, 21900000, '2016-03-28 00:00:00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', '', '', '', '', '', 0, 0, 0, '', 0),
(14, 4, 24, 2, 2, 0, 'manMan', 365436356, 6968989, '2017-03-03 00:00:00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', '', '', '', '', '', 0, 0, 0, '', 0),
(15, 4, 70, 1, 2, 0, 'manMan1', 689986899, 5463456, '2017-02-02 00:00:00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', '', '', '', '', '', 0, 0, 0, '', 0),
(16, 5, 68, 1, 3, 0, 'manmWoman', 4568578, 6547674, '2016-03-04 00:00:00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', '', '', '', '', '', 0, 0, 0, '', 0),
(17, 5, 69, 3, 3, 0, 'manmWoman1', 678578678, 235452435, '2017-01-01 00:00:00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', '', '', '', '', '', 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `tariffs_internet`
--

CREATE TABLE `tariffs_internet` (
  `id` int(11) NOT NULL,
  `tariffs_for_change` text,
  `title` text,
  `service_name` text,
  `price` int(11) DEFAULT NULL,
  `with_vat` text,
  `vat_percent` int(11) DEFAULT NULL,
  `speed_download` int(11) DEFAULT NULL,
  `speed_upload` int(11) DEFAULT NULL,
  `speed_limit_at` int(11) DEFAULT NULL,
  `aggregation` int(11) DEFAULT NULL,
  `burst_limit` int(11) DEFAULT NULL,
  `burst_threshold` int(11) DEFAULT NULL,
  `burst_time` int(11) DEFAULT NULL,
  `priority` text,
  `deleted` tinyint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `tariffs_internet`
--

INSERT INTO `tariffs_internet` (`id`, `tariffs_for_change`, `title`, `service_name`, `price`, `with_vat`, `vat_percent`, `speed_download`, `speed_upload`, `speed_limit_at`, `aggregation`, `burst_limit`, `burst_threshold`, `burst_time`, `priority`, `deleted`) VALUES
(1, '', 'First internet tarif', 'First best tarif', 222, '1', 0, 1111, 1111, 100, 1, 0, 0, 0, 'normal', 0),
(2, '', 'Seconf internet tarif', 'Seconf internet tarif', 1111, '1', 0, 2222, 2222, 100, 1, 0, 0, 0, 'normal', 0),
(3, '', 'Third internet tariff', 'Third internet tariff', 3333, '1', 0, 3333, 3333, 100, 1, 0, 0, 0, 'normal', 0);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `tariffs_internet`
--
ALTER TABLE `tariffs_internet`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
