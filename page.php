<?php

    const SERVER = 'localhost';
	const USER = 'root';
	const PASSWORD = '';
	const DATABASE = 'test_work';
	
	const DRIVER = 'mysql';
	
    try {
         $db = new PDO(DRIVER.":host=".SERVER.";dbname=".DATABASE."", USER, PASSWORD);
         $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         $db->exec("SET names utf8");
     } catch (PDOException $e) {
    die('Помилка підключення до БД: ' . $e->getMessage());
   }
   
   $year = isset($_POST['year']) ? (int)$_POST['year'] : date('Y');
   
   $query = $db->prepare("SELECT partners.name, SUM(statistics.in_bytes) AS download, SUM(statistics.out_bytes) AS upload
   FROM partners LEFT JOIN statistics ON partners.id = statistics.partner_id
   INNER JOIN tariffs_internet ON tariffs_internet.id = statistics.tariff_id
   WHERE YEAR(statistics.start_date) = ? GROUP BY title");
   $query->execute([$year]);
   
   $count = $query->rowCount();
   
   $names = [];
   $download_upload = [];
   
   while ($data = $query->fetch(PDO::FETCH_OBJ)){
   	
   	$names[] = htmlspecialchars($data->name);
   	$download_upload[] = (int)$data->download;
   	$download_upload[] = (int)$data->upload;
   	
   	}
   	
   	 ?>
  
<style>
td{
border:1px solid #000000;
}
</style>
Рік: <b><?= $year ?></b><br>
Результати пошуку: <b><?= $count ?></b><br>
<table>
<tr>
<td ROWSPAN=2><center>
	
 <form method="POST">
   <select name="year">
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    <option value="2015">2015</option>
    <option value="2014">2014</option>
    <option value="2013">2013</option>
    <option value="2012">2012</option>
    <option value="2011">2011</option>
    <option value="2010">2010</option>
   </select><br>
   <input type="submit" value="Пошук">
  </form>
</center></td>  
<?php

   if($count == 0){
   	die('Нічого не знайдено.');
   }
   
   ?>
	
<?php foreach($names as $name): ?>
<td colspan="2"><center><b><?= $name ?></b></center></td>
<?php endforeach ?>
</tr>
<tr>
<?= str_repeat('<td><center> Download (GB) </center></td><td><center> Upload (GB) </center></td>', $count); ?>
</tr>
<tr>
<td> Speed / Speed + </td>
<?php foreach($download_upload as $down_up): ?>
<td> <?= $down_up ?> </td>
<?php endforeach ?>
</tr>
<tr>
<td> Turbo / Turbo + </td>
<?php foreach($download_upload as $down_up): ?>
<td> <?= $down_up ?> </td>
<?php endforeach ?>
</tr>
</table>